from google.appengine.api import users
from google.appengine.ext import blobstore
from google.appengine.ext.webapp import blobstore_handlers

import os
from datetime import datetime

import webapp2
import jinja2

import rest
import models
import scans

class MainPage(webapp2.RequestHandler):
    
    def get(self):
        template = JINJA_ENVIRONMENT.get_template('index.html')
        self.response.write(template.render())


class CreateCollection(webapp2.RequestHandler):
    
    def get(self):
        user = users.get_current_user()
        if user:
            template = JINJA_ENVIRONMENT.get_template('new_collection.html')
            self.response.write(template.render())
        else:
            greeting = ('<a href="%s">Sign in or register</a>.' %
                        users.create_login_url('/'))
            self.response.out.write('<html><body>%s</body></html>' % greeting)
    
    def post(self):
        user = users.get_current_user()
        if user:
            collection = models.Collection()
            collection.user = user
            collection.name = self.request.get('name')
            collection.description = self.request.get('description')
            collection.create_date = datetime.now()
            collection.scan_date = datetime.strptime(self.request.get('scan_date'), '%Y-%m-%d')
            collection.put()
            
            self.response.out.write('Yay!')
        else:
            greeting = ('<a href="%s">Sign in or register</a>.' %
                        users.create_login_url('/'))
            self.response.out.write('<html><body>%s</body></html>' % greeting)


class Collections(webapp2.RequestHandler):
    
    def get(self):
        user = users.get_current_user()
        if user:
            q = models.Collection.all()
            q.filter('created_by', user)
            collections = q.run()
            
            template = JINJA_ENVIRONMENT.get_template('collections.html')
            self.response.write(template.render({'collections': collections, 'user':user}))
        else:
            greeting = ('<a href="%s">Sign in or register</a>.' %
                        users.create_login_url('/'))
            self.response.out.write('<html><body>%s</body></html>' % greeting)


class UploadScan(webapp2.RequestHandler):
    
    def get(self, collection_id):
        user = users.get_current_user()
        if user:
            upload_url = '/collections/%s/upload' % collection_id
            template = JINJA_ENVIRONMENT.get_template('upload_single.html')
            self.response.out.write(template.render({'upload_url': upload_url}))
        else:
            greeting = ('<a href="%s">Sign in or register</a>.' % users.create_login_url('/'))
            self.response.out.write('<html><body>%s</body></html>' % greeting)

    def post(self, collection_id):
        user = users.get_current_user()
        if user:
            collection = models.Collection.get_by_id(int(collection_id))
            raw_file = self.request.POST.get('file', None)
            file_blob = raw_file.file.read()
            scan = scans.create_scan(file_blob, raw_file.name, collection_id)
            scan.collection = collection
            scan.put()
        else:
            greeting = ('<a href="%s">Sign in or register</a>.' % users.create_login_url('/'))
            self.response.out.write('<html><body>%s</body></html>' % greeting)


class SingleUploadHandler(blobstore_handlers.BlobstoreUploadHandler):
    
    def post(self, collection_id):
        collection_id = long(collection_id)
        collection = models.Collection.get_by_id(collection_id)
        blob_info = self.get_uploads('file')[0]
        
        if not users.get_current_user():
            blob_info.delete()
            self.redirect(users.create_login_url("/"))
            return
        else:
            scan = models.Scan(collection = collection,
                               uploaded_by=users.get_current_user(),
                               blob = blob_info)
            scan.put()
            self.redirect('/collections/%s' % collection_id)


class ShowCollection(webapp2.RequestHandler):
    
    def get(self, collection_id):
        collection_id = long(collection_id)
        collection = models.Collection.get_by_id(collection_id)
        
        user = users.get_current_user()
        template = JINJA_ENVIRONMENT.get_template('collection.html')
        self.response.write(template.render({'collection': collection, 'user':user}))
        
    
# Setting up the application
JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.join(os.path.dirname(__file__), 'templates')),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True)

application = webapp2.WSGIApplication([
    ('/', MainPage),
    ('/collections/', Collections),
    ('/collections/create', CreateCollection),
    ('/collections/(\d+)/', ShowCollection),
    ('/collections/(\d+)/upload', UploadScan),
    ('/collections/(\d+)/upload_single', SingleUploadHandler),
    ('/rest/.*', rest.Dispatcher)
], debug=True)

rest.Dispatcher.base_url = '/rest'
rest.Dispatcher.add_models_from_module(models)