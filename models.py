import dicom
import tools

from google.appengine.ext import db
from google.appengine.ext import blobstore
import cloudstorage as gcs


def date_formater(val):
	if val and len(val)==8:
		return '%s-%s-%s' % (val[0:4], val[4:6], val[6:8])
	else:
		return val

def time_formater(val):
	if val and len(val)>=6:
		return '%s:%s:%s' % (val[0:2], val[2:4], val[4:6])
	else:
		return val


class Collection(db.Model):
	""" A collection of MRI scans belonging to a user. 
		This would typically represent a collection of
		scans in a CD
	"""
	created_by = db.UserProperty(auto_current_user=True)
	name = db.StringProperty()
	description = db.TextProperty()
	create_date = db.DateTimeProperty()
	scan_date = db.DateTimeProperty()


class Scan(db.Model):
	""" Represents an individual scan. It contains most
		of the important metadata along with the id of
		the scan file
	"""
	FIELDS = ['PatientName', 'PatientSex', 'PatientBirthDate', 'PatientID', 'AccessionNumber',
			  'InstitutionName', 'RequestingPhysician', 'StudyDate', 'StudyTime',
			  'BodyPartExamined', 'PatientPosition', 'StudyDescription']
	
	FIELDS_FORMATTER = {'PatientBirthDate':date_formater, 'StudyDate':date_formater, 'StudyTime':time_formater}
	
	collection = db.ReferenceProperty(Collection, collection_name='collection_scans')
	name = db.StringProperty()
	mri_file = db.StringProperty()
	png_file = db.StringProperty()
	uploaded_by = db.UserProperty()
	uploaded_at = db.DateTimeProperty(required=True, auto_now_add=True)
	
	
	
	def get_meta_data(self):
		""" Extracts metadata from the mri image file """
		raw_file = gcs.open(self.mri_file, 'r')
		mri = dicom.read_file(raw_file)
		
		meta_data = {}
		for field in Scan.FIELDS:
			try:
				val = getattr(mri, field)
				if val in [None, '']:
					val = 'N/A'
				elif field in Scan.FIELDS_FORMATTER:
					val = Scan.FIELDS_FORMATTER[field](val)
			except Exception as e:
				val = 'N/A'
				tools.print_stacktrace()
			meta_data[field] = val
			
		return meta_data
		
		


class ScanComment(db.Model):
	""" Comment made on a scan """
	scan = db.ReferenceProperty(Scan, collection_name='scan_comments')
	name = db.StringProperty()
	email = db.StringProperty()
	comment = db.TextProperty()