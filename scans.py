import dicom
import png

import models

import webapp2
import cloudstorage as gcs

import os
import png
import uuid
import shutil
import mritopng

from google.appengine.api import app_identity

RETRY_PARAMS = gcs.RetryParams(backoff_factor=1.1)

def create_scan(upload_blob, scan_name, collection_id):
	""" Takes a file blob of and MRI file and creates a
		scan object
	"""
	filename = str(uuid.uuid4())
	bucket_name = os.environ.get('BUCKET_NAME', app_identity.get_default_gcs_bucket_name())

	# Writing the dicom file
	mri_filename = '/' + bucket_name +  '/' + collection_id + '/' + filename + '.dicom'
	mri_file = gcs.open(mri_filename, 'w', retry_params=RETRY_PARAMS)
	mri_file.write(upload_blob)
	mri_file.close()

	# Writing the png file
	png_filename = '/' + bucket_name +  '/' + collection_id + '/' + filename + '.png'
	mri_file = gcs.open(mri_filename, 'r')
	png_file = gcs.open(png_filename, 'w', retry_params=RETRY_PARAMS, content_type='image/png')
	mritopng.mri_to_png(mri_file, png_file)
	png_file.close()

	# Creating the scan object
	scan = models.Scan()
	scan.name = scan_name
	scan.mri_file = mri_filename
	scan.png_file = png_filename

	return scan


class ScanToPNG(webapp2.RequestHandler):
	
	def get(self, scan_id):
		scan_id = long(scan_id)
		scan = models.Scan.get_by_id(scan_id)
		
		png_file = gcs.open(scan.png_file, 'r')
		self.response.content_type = 'image/png'
		shutil.copyfileobj(png_file, self.response.out)


class UploadScan(webapp2.RequestHandler):

	def get(self):
		self.response.out.write('<html><body>')
		self.response.out.write('<form action="/scans/upload" method="POST" enctype="multipart/form-data">')
		self.response.out.write("""Upload File: <input type="file" name="file"><br> <input type="submit"
			name="submit" value="Submit"> </form></body></html>""")

	def post(self):
		raw_file = self.request.POST.get('file', None)
		file_blob = raw_file.file.read()

		# Prepating gcs
		
		bucket_name = os.environ.get('BUCKET_NAME', app_identity.get_default_gcs_bucket_name())

		# Writing the gcs file
		mri_filename = '/' + bucket_name + '/est_file.mri'
		mri_file = gcs.open(mri_filename, 'w', retry_params=RETRY_PARAMS)
		mri_file.write(file_blob)
		mri_file.close()

		# Writing the png file
		png_filename = '/' + bucket_name + '/est_file.png'
		mri_file = gcs.open(mri_filename, 'r')
		png_file = gcs.open(png_filename, 'w', retry_params=RETRY_PARAMS,
						    content_type='image/png')
		mritopng.mri_to_png(mri_file, png_file)
		png_file.close()



		self.response.out.write('Good so far')

application = webapp2.WSGIApplication([
    ('/scans/(\d+).png', ScanToPNG),
    ('/scans/upload', UploadScan)
])