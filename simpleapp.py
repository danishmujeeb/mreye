import os
import scans
import models
import jinja2
import webapp2
from datetime import datetime

class ConvertMRI(webapp2.RequestHandler):
    
    def get(self, scan_id=None):
        
        if scan_id is None:
            data = {'upload_url': '/'}
            
            error_msg = self.request.get('error_msg')
            if error_msg and len(error_msg)>0:
                data['error_msg'] = error_msg
            
            template = JINJA_ENVIRONMENT.get_template('convert.html')
            
            self.response.write(template.render(data))
        else:
            scan_id = long(scan_id)
            scan = models.Scan.get_by_id(scan_id)
            
            template = JINJA_ENVIRONMENT.get_template('convert.html')
            
    
            data = {'upload_url': '/', 'scan_id': scan.key().id(), 'scan': scan}
            self.response.write(template.render(data))
    
    def post(self):        
        q = models.Collection.all()
        q.filter('name', 'QUICK_CONVERT')
        collections = q.run()
        
        collection = None
        for c in collections:
            collection = c
            break
        
        # Creating a collection if needed
        if collection is None:
            collection = models.Collection()
            collection.name = 'QUICK_CONVERT'
            collection.create_date = datetime.now()
            collection.put()
        
        # Storing the scan
        raw_file = self.request.POST.get('file', None)
        file_blob = raw_file.file.read()
        
        try:
            scan = scans.create_scan(file_blob, raw_file.name, str(collection.key().id()))
            scan.collection = collection
            scan.put()
            
            return webapp2.redirect('/scan/%s/' % scan.key().id())
        except Exception as e:
            return webapp2.redirect('/?error_msg=Could not convert MRI file: %s' % e)
        
        


# Setting up the application
JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.join(os.path.dirname(__file__), 'templates')),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True) 


application = webapp2.WSGIApplication([
    ('/', ConvertMRI),
    ('/scan/(\d+)/', ConvertMRI)
], debug=True)